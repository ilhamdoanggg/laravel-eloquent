<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $fillable = [
        'name',
        'ibukota',
    ];

    public function kabupaten()
    {
        return $this->hasMany(Kabupaten::class);
    }
}

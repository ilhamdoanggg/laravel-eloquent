<?php

use Illuminate\Database\Seeder;

class ProvinsiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provinsi = [
            [
                'name' => 'DKI Jakarta',
                'ibukota' => 'Jakarta',
            ],
            [
                'name' => 'Banten',
                'ibukota' => 'Serang',
            ],
            [
                'name' => 'Jawa Barat',
                'ibukota' => 'Bandung',
            ]
        ];

        foreach ($provinsi as $key => $data) {
            \App\Provinsi::create($data);
        }
    }
}

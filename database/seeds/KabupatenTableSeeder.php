<?php

use Illuminate\Database\Seeder;

class KabupatenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kabupaten = [
            [
                'provinsi_id' => '2',
                'name' => 'Cilegon',
            ],
            [
                'provinsi_id' => '2',
                'name' => 'Pandeglang',
            ]
        ];

        foreach ($kabupaten as $key => $data) {
            \App\Kabupaten::create($data);
        }
    }
}

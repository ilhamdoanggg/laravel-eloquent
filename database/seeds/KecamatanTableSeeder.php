<?php

use Illuminate\Database\Seeder;

class KecamatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kecamatan = [
            [
                'kabupaten_id' => '1',
                'name' => 'Jombang Kali',
            ]
        ];

        foreach ($kecamatan as $key => $data) {
            \App\Kecamatan::create($data);
        }
    }
}
